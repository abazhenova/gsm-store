import { GET_PRODUCTS_LIST } from './actionTypes';
import axios from 'axios';

export default function getProductsList() {
    return dispatch => {
        axios.get('http://localhost:4040/products')
            .then(res => {
                console.log('Products list ::', res.data);
                const products = res.data.map(product => {
                    product.note = 'none';
                    return product;
                });

                dispatch(getProductsListAsync(products));
                console.log('Products list :: 3333333333333333333333333')
            });
    }
}

function getProductsListAsync(products){
console.log('Products list :: 11111111111111111111')
    return {
        type: GET_PRODUCTS_LIST,
        payload: products
    };
}
