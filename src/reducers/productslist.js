import { GET_PRODUCTS_LIST,
    ADD_PRODUCT,
    UPDATE_PRODUCT,
    DELETE_PRODUCT } from '../actions/actionTypes';

export default function(state=[], action) {

    switch (action.type) {
        case GET_PRODUCTS_LIST:
            return action.payload;
            // return state;

        case ADD_PRODUCT:
            return [action.payload, ...state];

        case UPDATE_PRODUCT:
            return state.map(product => {
                if(product.title === action.payload.title) {
                    return action.payload;
                }
                return product;
            });

        case DELETE_PRODUCT:
            return state.filter(product => product.name !== action.payload.name);

        default:
            return state;
    }

}
