import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware} from 'redux'
import 'antd/dist/antd.css';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import App from './App';
import './index.css';
import reducer from './reducers';
import About from './About';




const store = createStore(reducer,  composeWithDevTools(applyMiddleware(thunk)));

// const addTrackBtn = document.querySelectorAll('.addTrack')[0];
// const list = document.querySelectorAll('.list')[0];
// const trackInput = document.querySelectorAll('.trackInput')[0];
//
//
//
// store.subscribe(() => {
//     console.log('subscribe', store.getState());
//     list.innerHTML = '';
//     trackInput.value = '';
//     store.getState().forEach(track => {
//         const li = document.createElement('li');
//
//         li.textContent = track;
//         list.appendChild(li);
//     })
// })



//
// // store.dispatch({ type: 'ADD_TRACK', payload: 'Smells like spirit' });
// // store.dispatch({ type: 'ADD_TRACK', payload: 'Enter Sandman' });
//
//
// addTrackBtn.addEventListener('click', () => {
//     const trackName = trackInput.value;
//     console.log('track name', trackName);
//     document.querySelectorAll('.trackInput')[0].value = '';
//     store.dispatch({ type: 'ADD_TRACK', payload: trackName });
// });


ReactDOM.render(
    <div>
        <Provider store={store}>
            <Router>
                <div>
                    <div>
                        <Link to="/">Tracks</Link>
                        <Link to="/about">About</Link>
                    </div>
                    <Route exact path="/" component={App}/>
                    <Route path="/about" component={About}/>
                </div>

            </Router>
            {/*<App />*/}
        </Provider>,
    </div>
,
  document.getElementById('root')
);
