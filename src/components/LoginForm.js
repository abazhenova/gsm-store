import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import axios from 'axios';
import {css} from 'aphrodite/no-important';
import styles from './FormStyles';
const FormItem = Form.Item;

class NormalLoginForm extends Component {
    state = {
        email: '',
        password: '',
    };

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                const {email, password} = values;
                axios({
                    url: 'http://localhost:4040/users/signin',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data:{email, password}
                }).then((data) => {
                    console.log(data, 'login data')
                }).catch((e) => {console.log(e, '.............login failed')})
            }
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form
                onSubmit={this.handleSubmit}
                className={css(styles.loginForm) + " login-form"}
            >
                <FormItem>
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: 'Please input your username!' }],
                    })(
                        <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="email" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
                        <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('remember', {
                        valuePropName: 'checked',
                        initialValue: true,
                    })(
                        <Checkbox>Remember me</Checkbox>
                    )}
                    <a className={css(styles.loginFormForgot) + " login-form-forgot"} href="" >Forgot password</a>
                    <Button type="primary" htmlType="submit" className={css(styles.loginButton) + " login-form-button"}>
                        Log in
                    </Button>
                </FormItem>
            </Form>
        );
    }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm);
export default WrappedNormalLoginForm;
