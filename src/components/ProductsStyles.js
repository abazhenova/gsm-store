import {StyleSheet} from 'aphrodite/no-important';

export default StyleSheet.create({
    container: {
        background: '#bfbea7'
    },
    productCard: {
        margin: '0 auto 10px',
    },

    title: {
        fontWeight: '600',
        fontSize: '20px',
        textAlign: 'center',
        margin: '4px 0',
        color: '#212121'
    },

    productImage: {
        maxWidth: '100%',
        maxHeight: '225px',
    },

    price: {
        fontWeight: '600',
        fontSize: '18px',
        textAlign: 'center',
        margin: '8px 0',
        color: '#880E4F'
    }
})
