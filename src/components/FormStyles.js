import {StyleSheet} from 'aphrodite/no-important';

export default StyleSheet.create({
    loginForm: {
        width: '488px'
    },
    loginFormForgot: {
        float: 'right'
    },
    loginButton: {
        width: '100%'
    }
})
