import React, {Component, PropTypes} from 'react';
import {Modal, Button, Form, Icon, Input, Checkbox, Avatar, Row, Col,  Radio} from 'antd';
import logo from '../logo.svg';
import '../App.css';
import Menu from './Menu';
import LoginForm from './LoginForm';
import RegisterForm from './RergisterForm';
import {css} from 'aphrodite/no-important';
import styles from './HeaderStyles';

const FormItem = Form.Item;


//============
const LoginForm2 = Form.create()(
    (props) => {
        const { visible, onCancel, onLogin, form } = props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                visible={visible}
                title="Login"
                okText="Login"
                cancelText="Cancel"
                onCancel={onCancel}
                onOk={onLogin}
            >
                <Form layout="vertical">
                    <FormItem>

                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: 'Please input your username!' }],
                    })(
                        <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="email" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
                        <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('remember', {
                        valuePropName: 'checked',
                        initialValue: true,
                    })(
                        <Checkbox>Remember me</Checkbox>
                    )}
                    <a className={css(styles.loginFormForgot) + " login-form-forgot"} href="" >Forgot password</a></FormItem>
                </Form>
            </Modal>
        );
    }
);
//==========



class Header extends Component {
    state = {
        modalLoginVisible: false,
        modalRegisterVisible: false,
        visible: false,
    }

    setModalLoginVisible(modalLoginVisible) {
        this.setState({modalLoginVisible});
    }

    setModalRegisterVisible(modalRegisterVisible) {
        this.setState({modalRegisterVisible});
    }
    //
    showModal = () => {
        this.setState({ visible: true });
    }
    handleCancel = () => {
        this.setState({ visible: false });
    }
    handleLogin = () => {
        const form = this.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);
            form.resetFields();
            this.setState({ visible: false });
        });
    }
    saveFormRef = (form) => {
        this.form = form;
    }

    //
    render() {
        return (
            <div className="Header">
                <div className="App-header">
                    <Row>
                        <Col xs={{span: 24}} md={{span: 16}}>
                            <h1 className={css(styles.headerTitle)} >GSM Store</h1>
                        </Col>
                        <Col xs={{span: 24}} md={{span: 8}}>
                            <div className={css(styles.topUserProfile)}>
                                <Avatar size="small" icon="user"/>
                                <span className={css(styles.topUserlink)}
                                      // onClick={() => this.setModalLoginVisible(true)}>Login</span>
                                      onClick={this.showModal }>Login</span>
                                or
                                <span className={css(styles.topUserlink)} onClick={() => this.setModalRegisterVisible(true)}>Register</span>
                            </div>
                        </Col>
                    </Row>
                    <LoginForm2
                        ref={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onLogin={this.handleLogin}
                    />
                    <div>{this.props.items.map((item, index) =>
                        <a href={item.link} key={index}>{item.label}</a>
                    )}</div>

                </div>
                <Modal
                    title="Login"
                    style={{top: 20}}
                    visible={this.state.modalLoginVisible}
                    footer={null}
                    onOk={() => this.setModalLoginVisible(false)}
                    onCancel={() => this.setModalLoginVisible(false)}
                >
                    <LoginForm/>
                </Modal>
                <Modal
                    title="Register"
                    wrapClassName="vertical-center-modal"
                    visible={this.state.modalRegisterVisible}
                    footer={null}
                    onOk={() => this.setModalRegisterVisible(false)}
                    onCancel={() => this.setModalRegisterVisible(false)}
                >
                    <RegisterForm/>
                </Modal>
                <Menu/>
            </div>
        );
    }

}

export default Header;
