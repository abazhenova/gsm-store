import {StyleSheet} from 'aphrodite/no-important';

export default StyleSheet.create({
    headerTitle: {
      textAlign: 'left',
      fontSize: '2rem',
      color: '#4FC3F7'
    },
    topUserProfile: {
        float: 'right',
        fontSize: '1.1rem',
        lineHeight: '30px',
        display: 'flex',
        alignItems: 'center'
    },
    topUserlink: {
        display: 'inline-block',
        margin: '0 8px',
        cursor: 'pointer',
        color: '#64B5F6'
    }

})
