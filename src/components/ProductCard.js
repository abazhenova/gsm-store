import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {css} from 'aphrodite/no-important';
import styles from './ProductsStyles';
import { Card, Button,  Icon , Col, Rate } from 'antd';


class ProductCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        const product = this.props.product;
        const title = product.title;
        return (
            <Col xs={{ span: 12}} md={{ span: 6}}>
                <Card bordered={false}className={css(styles.productCard)}>


                    <div className="card-header">
                        <figure data-tooltip={title}>
                            <img className={css(styles.productImage)}  alt={title} src={product.picture} />
                        </figure>
                        <h4 className={css(styles.title)}>{product.title}</h4>
                    </div>
                    <div>
                        <Rate disabled allowHalf value={Math.round(product.rating *2) /2} />  {product.rating && <span className="ant-rate-text">{product.rating}</span>}
                    </div>

                    <div className={css(styles.price)}>
                        ${product.price}
                    </div>
                    <Button
                        type="primary"
                        icon="shopping-cart"
                        className="btn btn-clear tooltip"
                        data-tooltip="Add to Cart."
                        >Add to Cart</Button>

                </Card>
            </Col>

        );
    }
}

//connects redux actions to props
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(null, mapDispatchToProps)(ProductCard);
