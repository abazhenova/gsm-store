// import React, {Component, PropTypes} from 'react';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import logo from './logo.svg';
import {Card, Col, Row} from 'antd';
import {css} from 'aphrodite/no-important';
import './App.css';
import styles from './AppStyles';
import Header from './components/Header.js';
import Dropdown from './Dropdown.js';
import RegistrationForm from './RegistrationForm';
import {connect} from 'react-redux'
import {getTracks} from './actions/tracks'
import getProductsList from './actions/getProductsList';
import ProductCard from './components/ProductCard';

const menu = [
    {
        link: '/articles',
        label: 'Articles'
    },
    {
        link: '/contacts',
        label: 'Contacts'
    },
    {
        link: '/posts',
        label: 'Posts'
    }
];

// const App = ({tracks, products, onAddTrack, onFindTrack, onGetTracks, onGetProducts, mapStateToProps}) => {
// let testInput = '';
// let trackInput = '';
// let searchInput = '';
//
//
// const submit = () => {
//     console.log('submit', testInput.value);
// };
//
// const addTrack = () => {
//     console.log('addTrack', trackInput.value);
//     onAddTrack(trackInput.value);
//     trackInput.value = '';
// }
//
// const findTrack = () => {
//     console.log('findTrack', searchInput.value);
//     onFindTrack(searchInput.value);
// };
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        this.props.getProductsList();
    }

    renderProducts() {
        if (this.props.products) {
            return this.props.products.map(product => {
                // return (
                //     <div key={product.title} product={product}>{product.title} </div>
                // )
                return <ProductCard key={product._id} product={product}/>;
            });
        } else {
            // return <LoadingSpinner />;
        }
    }

    render() {
        return (

            <div className="App">
                <Header items={menu}/>

                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
                </p>
                <Dropdown/>
                <RegistrationForm/>

                <br/>
                {/*<input type="text"  ref={(input) => {*/}
                {/*this.testInput = input;*/}
                {/*}}/>*/}
                {/*<button onClick={this.submit.bind(this)}>Submit</button>*/}

                <br/><br/>
                <Row gutter={16}>
                    {this.renderProducts()}
                </Row>

                {/*<div>*/}
                {/*<button onClick={onGetTracks}>Get tracks</button>*/}
                {/*</div>*/}

                <div>
                    {/*<button onClick={onGetProducts}>Get products</button>*/}
                    {/*<ul>*/}
                    {/*{products.map((product, index) =>*/}
                    {/*<li key={index}>{product.title}</li>*/}
                    {/*)}*/}
                    {/*</ul>*/}

                </div>

                <div className={css(styles.container)}>
                    {/*<input type="text" ref={(input) => {*/}
                    {/*trackInput = input;*/}
                    {/*}}/>*/}
                    {/*<button onClick={addTrack}>Add track</button>*/}
                    {/*<ul>*/}
                    {/*{tracks.map((track, index) =>*/}
                    {/*<li key={index}>{track.name}</li>*/}
                    {/*)}*/}
                    {/*</ul>*/}

                    <div>
                        {/*<input type="text" ref={(input) => {*/}
                        {/*searchInput = input*/}
                        {/*}}/>*/}
                        {/*<button onClick={findTrack}>Find track</button>*/}
                    </div>
                </div>
                <div className={css(styles.square)}/>
            </div>
        );

    }
}

// export default connect(
//     state => ({
//         tracks: state.tracks.filter(track => track.name.includes(state.filterTracks)),
//         products: state.products
//
//     }),
//     dispatch => ({
//         onGetTracks: () => {
//             dispatch(getTracks());
//         },
//         onGetProducts: () => {
//             dispatch(getProductsList());
//         },
//         onAddTrack: (name) => {
//             const payload = {
//                 id: Date.now().toString(),
//                 name
//             }
//             dispatch({type: 'ADD_TRACK', payload})
//         },
//         onFindTrack: (name) => {
//             dispatch({type: 'FIND_TRACK', payload: name})
//         }
//     })
// )(App);

//connects root reducer to props
function mapStateToProps(state) {
    return {
        products: state.products,
    }
}

//connects redux actions to props
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getProductsList: getProductsList,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
